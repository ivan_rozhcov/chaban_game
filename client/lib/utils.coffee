@coordsRelativeToElement = (element, event) ->
  elem = $(element)
  offset = elem.offset()
  x = event.pageX - offset.left + elem.scrollLeft()
  y = event.pageY - offset.top + elem.scrollTop()
  x: x
  y: y
