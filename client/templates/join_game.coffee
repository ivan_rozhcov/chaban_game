Template.joinGame.helpers
  second_player: ->
    return if @from_user == Meteor.userId() then @to_user else @from_user


Template.joinGame.onRendered ->
  callback = (buttonNumber) ->
    console.log 'callback', arguments
    Router.go 'game.menu'

  if Meteor.isCordova
    window.playerRefusesAlert = ->
      navigator.notification.alert( TAPi18n.__("invites.playerRefuse"), callback, "Alert")
  else
    window.playerRefusesAlert = ->
      alert 'The other player refuses to play!'
      Router.go 'game.menu'

  @autorun (c)->
    data = Template.currentData()
    if data
      console.log 'data', data
      # если игрок отказался от приглашения
      if data.closed
        playerRefusesAlert()
      unless (data.game_id)
        return
      c.stop()
      Router.go 'game.grid',  {_id: data.game_id}