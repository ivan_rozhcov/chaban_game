Template.userList.onCreated ->
  instance = @
  instance.autorun ->
    # get the limit
#    console.log 'Asking for NodeTypes'
    # subscribe to the posts publication
    subscription = instance.subscribe('invite_users', Session.get('search_query'))
    # if subscription is ready, set limit to newLimit
#    if subscription.ready()
#      console.log '> Received node_types. \n\n'
#    else
#      console.log '> Subscription is not ready yet. \n\n'
    return

  
Template.userList.helpers
  users: ->
    query = Session.get('search_query')
    if query
      Meteor.users.find(username: {$regex: query.toLowerCase()}, _id: $ne: Meteor.userId())
    else
      Meteor.users.find(_id: $ne: Meteor.userId())

Template.userList.events
  'click .invite_button' : (e,template) ->
    userId = @_id
    inviteId = sendInvite(userId, Meteor.userId())

    Router.go 'game.join_game',  {_id: inviteId}

  'click .send_invite_button' : (e,template) ->
    userId = @_id
    inviteId = sendInvite(userId, Meteor.userId())
    Meteor.call('inviteUser', Meteor.userId(), userId)
    Router.go 'game.join_game',  {_id: inviteId}

#custom filters
Template.greaterThanFilter.helpers
  getValue: ->
    Session.get('search_query')

Template.greaterThanFilter.events 'keyup #greater-than-filter': _.debounce(((event, template) ->
  input = $(event.currentTarget).val()
  Session.set('search_query', input)
  return
), 200)