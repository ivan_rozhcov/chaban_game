Template.newGameMenu.events
  'click .configure-game' : (e,t) ->
    Router.go 'game.configure'
  'click .play-with-robot' : (e,t) ->
    robot = Meteor.users.findOne('emails.address': 'robot_1')
    inviteId = sendInvite(robot._id, Meteor.userId())
    Router.go 'game.join_game',  {_id: inviteId}