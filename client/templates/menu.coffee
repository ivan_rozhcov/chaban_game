Template.menu.events
  'click .create-game' : (e,t) ->
    Router.go 'game.new_game_menu'
    e.stopPropagation()
  'click .join-game' : (e,t) ->
    Router.go 'game.join_game'
  'click .go-score' : (e,t) ->
    Router.go 'game.score'
  'click .go-tutorial' : (e,t) ->
    Router.go 'game.tutorial'
  'click .change-name' : (e,t) ->
    Router.go 'game.select_name'
  'click .sign_out' : (e,t) ->
    Meteor.logout()