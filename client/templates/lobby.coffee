Template.gameLobby.onRendered ->
  #делаем это тут,а не в router - тк там работает реактивно
  lobby = Lobby.findOne()
  Lobby.update lobby._id, $addToSet: wait_list: Meteor.userId()
  @autorun (c)->
    userId = Meteor.userId()
    activeInvite = Invites.findOne($or: [{to_user: userId, from_user: $exists: true}, {from_user: userId, to_user: $exists: true}], viewed: $ne: true)
    if activeInvite
      c.stop(activeInvite)
      Router.go 'game.join_game',  {_id: activeInvite._id}

Template.gameLobby.events
  'click .abort-game': (e,t) ->
    #покидаем очередь ожидания
    lobby = Lobby.findOne()
    Lobby.update lobby._id, $pull: wait_list: Meteor.userId()
    console.log 'lobby pull'