@callback = null
@exitGameConfirm = null

Template.grid.onCreated( ->
  console.log 'onRendered'
  Meteor.call 'clearAllInvites'
  if Template.instance()?.data

    # confirm
    game = Template.instance().data

    callback = (buttonNumber) ->
      console.log 'callback', arguments
      if buttonNumber is 1
        obj = is_force_end: true
        key = "#{Meteor.userId()}_fire_points"
        obj[key] = []
        otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
        otherkey = "#{otherUserId}_fire_points"
        obj[otherkey] = game["#{Meteor.userId()}_points"]
        console.log 'update game', game, obj
        Game.update game._id, $set: obj
        return
    if Meteor.isCordova
      window.exitGameConfirm = ->
        navigator.notification.confirm(TAPi18n.__("game.endGameConfirm"), callback, "Alert")
    else
      window.exitGameConfirm = ->
        res = confirm TAPi18n.__("game.endGameConfirm")
        if res
          obj = {is_force_end: true, reason: TAPi18n.__("game.userQuit")}
          key = "#{Meteor.userId()}_fire_points"
          obj[key] = []
          otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
          otherKey = "#{otherUserId}_fire_points"
          obj[otherKey] = game["#{Meteor.userId()}_points"]
          Game.update game._id, $set: obj


  Meteor.users.find('status.online': true).observe
    added: (id) ->
      console.log 'online', id
      # id just came online
      # если был таймер отключения убираем его
      if not(id is Meteor.userId()) and window.OPPONENT_TIMER_ID
        Meteor.clearInterval CLIENT_TIMER_ID
      return
    removed: (id) ->
      # id just went offline
      console.log 'offline', id
      unless game.game_mode is "ended"
        unless id is Meteor.userId()
          # если таймер уже идет ничего не делаем, чтобы юзер не мог скакать "туда сюда"
          unless window.OPPONENT_TIMER_ID
            window.CLIENT_TIMER_ID = Meteor.setInterval((->
              console.log 'user oflline', id
              #отчищаем, интервал на всякий случай
              Meteor.clearInterval CLIENT_TIMER_ID

              message = 'Opponent disconnected from server!'

              callback = ->
                obj = {is_force_end: true, reason: TAPi18n.__("game.userDisconnected")}

                key = "#{Meteor.userId()}_fire_points"
                obj[key] = []
                otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
                otherkey = "#{otherUserId}_fire_points"
                obj[otherkey] = []
                Game.update game._id, $set: obj
                Router.go 'game.menu'

              if Meteor.isCordova
                window.playerDisconnectedAlert = ->
                  navigator.notification.alert(message, callback, "Alert")
              else
                window.playerDisconnectedAlert = ->
                  alert message
                  callback()


              playerDisconnectedAlert()

          ), 5000)
      return


    #подписываемся на изменения игры

    query = Game.find(Template.instance().data._id)
    #    TODO как сделать реактивным?
    game = Game.findOne(Template.instance().data._id)
    otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
    otherUserPoints = otherUserId + '_fire_points'
    yourSetPoints = Meteor.userId() + '_points'
    Session.set('fired_points', _.clone(game[otherUserPoints]))
    console.log 'new shot', Session.get('fired_points')
    query.observeChanges
      changed: (id, fields) ->
        game = Game.findOne(id)
        console.log 'changed', fields
#        console.log fields?["#{otherUserId}_fire_points"]?.length
        if fields?.game_mode is GAME_MODE_ENDED
          # move screen back
          $('.game-table').animate({"zoom": 0.5, "width": '100%', "margin-right": '0px', ".margin-left": '0px'})
          console.log 'doc updated', fields
          if getYouScore(game, game.users[0]) == game.tap_times
            championId = game.users[0]
            looserId = game.users[1]
          else
            championId = game.users[1]
            looserId = game.users[0]
          console.log 'game over championId', championId, 'looserId', looserId
          if Meteor.userId() is championId
            getSoundForEvent('win')
          else
            getSoundForEvent('fail')
          return
        else if game.game_mode is GAME_MODE_STARTING
          console.log 'GAME_MODE_STARTING'
          $('.game-table').animate {
            'zoom': '1'
            'width': '200%'
            top: '5%'
            "margin-right": $(window).width() + 'px'
            "margin-left": '-' + $(window).width() + 'px'
          }, complete: () ->
            $(this).css 'transform', 'translate(0,5%)'
          $('.board-name').animate {
            'zoom': '1'
            'width': '200%'
            top: '10%'
            "margin-right": $(window).width() + 'px'
            "margin-left": '-' + $(window).width() + 'px'
          }, complete: () ->
            $(this).css 'transform', 'translate(0,10%)'
        else if game.game_mode is GAME_MODE_PLAYING
          if (fields?[otherUserPoints]?.length > 0)
  #          console.log 'fire!'
            getSoundForEvent('fire')
          else if fields?.user_turn
            if fields?.user_turn is Meteor.userId()
              $('.game-table').animate({"margin-right": '0px', "margin-left": '0px'}) #go left
              $('.board-name').animate({"margin-right": '0px', "margin-left": '0px'}) #go left
            else
              $('.game-table').animate({"margin-right": $(window).width() + 'px', "margin-left": '-' + $(window).width() + 'px'}) #go right
              $('.board-name').animate({"margin-right": $(window).width() + 'px', "margin-left": '-' + $(window).width() + 'px'}) #go right
            Session.set 'game_message', null
#          console.log 'search for a new point'
#          console.log 'yourSetPoints', game[yourSetPoints]
#          console.log 'otherUserPoints', fields[otherUserPoints]
#          console.log 'fired_points', Session.get('fired_points')

          if fields[otherUserPoints] and fields[otherUserPoints].length
            console.log 'true'
            # find new poind that is not in fired_points (previously fired)
            hit = false
            for point in fields[otherUserPoints]
#              console.log 'try to find', point
              if  _.findWhere(Session.get('fired_points'), point)
                continue
                #first time we didin't find - means we found a new point
              else
                break

#            console.log 'found point!', point

            if point and _.findWhere(game[yourSetPoints], point)
              Session.set 'game_message', TAPi18n.__("game.hit")
              getSoundForEvent('hit_player')
              hit = true

            Session.set('fired_points', _.clone(fields[otherUserPoints]))
            unless hit
              Session.set 'game_message', TAPi18n.__("game.missed")

    Meteor.setTimeout((->
      console.log('game update')
      Game.update game._id, $set: test: 'best'), 1000)

)

Template.grid.helpers
  youScore: ->
    getYouScore(@, Meteor.userId())

  otherScore: ->
    game = @
    otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
    selfSetPoints = "#{Meteor.userId()}_points" or []
    otherFiredPoints = "#{otherUserId}_fire_points" or []
    _.filter(game[selfSetPoints], (itm) -> _.findWhere(game[otherFiredPoints], itm)).length

  getGameMessage: ->
    Session.get 'game_message'

  getOpponent: ->
    Meteor.users.findOne(getOtherUserId(@))
  isYourTurn: ->
    @user_turn == Meteor.userId()
  gridCols: (game) ->
    GAME_MAX_ROWS/game.game_cols

  getGridMyClass: (game) ->
    if game.game_mode == GAME_MODE_STARTING
      selfSetPoints = "#{Meteor.userId()}_points" or []
      if _.findWhere(game[selfSetPoints], @)
        return 'cell_chosen'

    else
      otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())

      selfSetPoints = "#{Meteor.userId()}_points" or []
      selfFiredPoints = "#{Meteor.userId()}_fire_points" or []

      otherFiredPoints = "#{otherUserId}_fire_points" or []
      otherSetPoints = "#{otherUserId}_points" or []
      # в свой ход, показываем куда стрелял и куда попал в противника

      # попал в чужие
      if _.findWhere(game[selfFiredPoints], @) and _.findWhere(game[otherSetPoints], @)
        return 'cell_chosen'
      if _.findWhere(game[selfFiredPoints], @)
        return 'cell_choosing'

  getGridOpponentClass: (game) ->
    if game.game_mode == GAME_MODE_STARTING
      selfSetPoints = "#{Meteor.userId()}_points" or []
      if _.findWhere(game[selfSetPoints], @)
        return 'cell_chosen'

    else
      otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())

      selfSetPoints = "#{Meteor.userId()}_points" or []
      selfFiredPoints = "#{Meteor.userId()}_fire_points" or []

      otherFiredPoints = "#{otherUserId}_fire_points" or []
      otherSetPoints = "#{otherUserId}_points" or []
      # в ход противника показываем свои и куда стрелял противник
      if _.findWhere(game[otherFiredPoints], @) and _.findWhere(game[selfSetPoints], @)
        return 'cell_guessed'
      if _.findWhere(game[otherFiredPoints], @)
        return 'cell_choosing'
      if _.findWhere(game[selfSetPoints], @)
        return 'cell_chosen'

  gameEnded: ->
    @game_mode == GAME_MODE_ENDED
  gameStarted: ->
    @game_mode == GAME_MODE_STARTING

  isWaitForYour: ->
    getUserPoints(@, Meteor.userId()).length < @tap_times

  getRemainingPlayerPoints: ->
    @tap_times - getUserPoints(@, Meteor.userId()).length

  isYouWin: ->
    getYouScore(@, Meteor.userId()) == @tap_times
  delta: ->
    game = @
    Math.abs(getYouScore(game, game.users[0]) - getYouScore(game, game.users[1]))


Template.grid.events
  'click .cell' : (e,template) ->
    row = @row
    col = @col
    game = template.data

    objStr = "#{Meteor.userId()}_points"
    userPoints = game[objStr] or []

    if game.game_mode == GAME_MODE_STARTING
      if userPoints.length == game.tap_times
        console.log 'thats enough'
        getSoundForEvent('cant_click')
      else if _.findWhere(userPoints, @)
#        console.log 'tap'
        getSoundForEvent('cant_click')
      else
        query = {}
        query[objStr] = col: col, row: row
        Game.update game._id, $addToSet: query

        $('#thumb-image').css('top', "#{event.pageY - 30}px").css('left', "#{event.pageX - 25}px").fadeIn().fadeOut()
        getSoundForEvent('fire')

    else if game.game_mode == GAME_MODE_PLAYING and game.user_turn == Meteor.userId()
      objStr = "#{Meteor.userId()}_fire_points"
      userPoints = game[objStr] or []
      if game.turn_points_tap - game.user_turn_points <= 0
#        console.log 'that's enough'
        getSoundForEvent('cant_click')
      else if _.findWhere(userPoints, @)
#        console.log 'you fired here already'
        getSoundForEvent('cant_click')
      else
        query = {}
        query[objStr] = col: col, row: row
        Game.update game._id, $addToSet: query, $inc: user_turn_points: 1

        $('#thumb-image').css('top', "#{event.pageY - 30}px").css('left', "#{event.pageX - 25}px").fadeIn().fadeOut()
        getSoundForEvent('fire')
        otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
        otherSetPoints = "#{otherUserId}_points" or []
        if _.findWhere(game[otherSetPoints], {col: col, row: row})
          console.log 'your hit!'
          getSoundForEvent('hit_oponent')
          Session.set 'game_message', TAPi18n.__("game.hit")
        else
          Session.set 'game_message', TAPi18n.__("game.missed")

    else
#      console.log('not your turn!')
      getSoundForEvent('cant_click')


  'click .go-menu': (e,t) ->
    e.stopPropagation()
    Meteor.setTimeout(( -> Router.go('game.menu')), 10)

  'click .quit-game': (e,t) ->
    e.stopPropagation()
    exitGameConfirm()

  'click .go-rating': (e,t) ->
    Router.go 'game.score'

  'click .create-game' : (e,template) ->
    game = @
    #TODO сейчас при пересоздании игры ее параметры не сохранятся - ну и фиг с ним )
    otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
    inviteId = sendInvite(otherUserId, Meteor.userId())
    Router.go 'game.join_game',  {_id: inviteId}


Template.getGameMessage.helpers
  pointsToSelect: ->
    objStr = "#{Meteor.userId()}_points"
    userPoints = @[objStr] or []
    @tap_times - userPoints.length

  isYourTurn: ->
    @user_turn == Meteor.userId()

  turnPointsLeft: ->
    @turn_points_tap - @user_turn_points

Template.getGameScore.helpers
  youScore: ->
    getYouScore(@_id, Meteor.userId())

  otherScore: ->
    game = @
    otherUserId = _.find(game.users , (_id) -> _id != Meteor.userId())
    selfSetPoints = "#{Meteor.userId()}_points" or []
    otherFiredPoints = "#{otherUserId}_fire_points" or []
    _.filter(game[selfSetPoints], (itm) -> _.findWhere(game[otherFiredPoints], itm)).length


relativeTime = (timeAgo) ->
  diff = moment.utc(TimeSync.serverTime() - timeAgo)
  Number(diff.format("s"))

Template.showTimer.helpers
  timerValue: ->
    lastActivity = @turn_time
    if lastActivity?
      delta = @seconds_to_turn  - relativeTime lastActivity
      if delta > @seconds_to_turn
        ''
      else if delta < 0
        ''
      else
        delta
    else
      ''
