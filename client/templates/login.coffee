Template.login.events
  #https://github.com/meteor/meteor/wiki/OAuth-for-mobile-Meteor-clients
  #TODO callbacks в том числе ошибки
  'click .login-with-google' : (e,t) ->
    Meteor.loginWithGoogle(loginStyle: "redirect")
  'click .login-with-vk' : (e,t) ->
    Meteor.loginWithVk(loginStyle: "redirect")
  'click .login-with-fb' : (e,t) ->
    Meteor.loginWithFacebook(loginStyle: "redirect")