Template.invites.helpers
  newInvites: ->
    Invites.find(to_user: Meteor.userId(), viewed: $ne: true)


Template.invites.events
  'click .notification' : (e,template) ->
    Invites.update @_id, $addToSet: invite_accepted: Meteor.userId()
    Router.go 'game.join_game', _id: @_id
  'click .close' : (e,template) ->
    Invites.update @_id, $set: {viewed: true, closed: true}