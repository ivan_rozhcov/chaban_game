Template.configureGame.helpers
  GAME_ROWS: ->
    GAME_ROWS
  GAME_COLS: ->
    GAME_COLS
  TAP_TIMES: ->
    TAP_TIMES
  TURN_POINTS_TAP: ->
    TURN_POINTS_TAP
  SECONDS_TO_TURN: ->
    SECONDS_TO_TURN
  EXTRA_TURN_FOR_LOOSER: ->
    EXTRA_TURN_FOR_LOOSER
  CLEAR_EVERY_TURN: ->
    CLEAR_EVERY_TURN

Template.configureGame.events
  'click button' : (e,template) ->
    $('#demo'). serializeArray()
    gameObj = _.object(_.map($('#demo'). serializeArray(), _.values))

    _.each gameObj, (value, key, obj) ->
      if value == 'on'
        obj[key] = true
      else
        obj[key] = Number(value)

    e.preventDefault()
    Meteor.call 'createGame', Session.get('other_user'), Meteor.userId(), gameObj, (err, gameId) ->
      sendInvite(Session.get('other_user'), Meteor.userId(), gameId)
      Router.go 'game.grid',  {_id: gameId}