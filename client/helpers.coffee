Template.registerHelper "score", () ->
  user = Meteor.user()
  if user
    user.won_games

Template.registerHelper "eq", (x1, x2) ->
  x1 is x2

Template.registerHelper "isRobot", () ->
  @emails && @emails[0].address is 'robot_1'

Template.registerHelper "GAME_MODE_STARTING",  ->
  GAME_MODE_STARTING

Template.registerHelper 'avatar', (user) ->
  if user
    if user?.services?.google?.picture?
      user.services.google.picture
    else if user?.services?.facebook?.id?
      "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=small"
    else if user?.services?.vk?.photo?
      user.services.vk.photo
    else if user?.profile?.avatar_url?
      user.profile.avatar_url
    else
      if user?.emails? and user.emails[0]
        email = user.emails[0].address
      else if user?.services?.google?
        email = user.services.google.email
      Gravatar.imageUrl(email, {size: 48})

Template.registerHelper 'isOdd', (numb) ->
  not (numb % 2)