getUserLanguage = ->
# Put here the logic for determining the user language
  'ru'

Meteor.startup ->
  Session.set 'showLoadingIndicator', true
  TAPi18n.setLanguage(getUserLanguage()).done(->
    Session.set 'showLoadingIndicator', false
    return
  ).fail (error_message) ->
# Handle the situation
    console.log error_message
    return

  #тип события : путь в звуку
  @soundEvents =
    fire: new Howl (urls: ['/sounds/tap.wav'] )
    cant_click: new Howl (urls: ['/sounds/dull.wav'] )
    select: new Howl (urls: ['/sounds/select.wav'] )
    back: new Howl (urls: ['/sounds/back.wav'] )
    win: new Howl (urls: ['/sounds/win.wav'] )
    fail: new Howl (urls: ['/sounds/fail.mp3'] )
    hit_player: new Howl (urls: ['/sounds/hit.wav'] )
    hit_oponent: new Howl (urls: ['/sounds/hit_oponent.wav'] )

  @isSoundEndbledForUser = ->
  #  Meteor.userId().profile.sounds_enabled
    true

  @getSoundForEvent  = (eventType) ->
    sound = soundEvents[eventType]
    if sound and isSoundEndbledForUser()
      sound.play()
  if Accounts.ui?
    Accounts.ui.config({
      passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL'
    });

onDeviceReady = ->
  document.addEventListener 'backbutton', ((e) ->
    console.log('backbutton')
    e.preventDefault()
    return
  ), false
  return

document.addEventListener 'deviceready', onDeviceReady, false

Push.Configure({
  android: {
      senderID: 1234567,
      alert: true,
      badge: true,
      sound: true,
      vibrate: true,
      clearNotifications: true
  },
  ios: {
    alert: true,
    badge: true,
    sound: true
  }
});