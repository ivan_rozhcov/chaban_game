App.info({
    name: 'ChabTap',
    description: 'A quick thumb game',
    version: '1.0.7',
    author: 'Ivan133 studio',
    email: 'support@chabtap.ru',
    website: 'http://chabtap.ru/'
});

// iphone_2x (120x120)
// iphone_3x (180x180)
// ipad (76x76)
// ipad_2x (152x152)
// ipad_pro (167x167) not!
// ios_settings (29x29)
// ios_settings_2x (58x58)
// ios_settings_3x (87x87) not!
// ios_spotlight (40x40)
// ios_spotlight_2x (80x80)

// android_mdpi (48x48) v
// android_hdpi (72x72) v
// android_xhdpi (96x96) v
// android_xxhdpi (144x144) v
// android_xxxhdpi (192x192) v

App.icons({
    'iphone_2x': 'resources/icons/ios_120x120.png',
    'iphone_3x': 'resources/icons/ios_180x180.png',
    'ipad': 'resources/icons/ios_76x76.png',
    'ipad_2x': 'resources/icons/ios_152x152.png',
    'ipad_pro': 'resources/icons/ios_152x152.png',

    'ios_settings': 'resources/icons/icon-29x29.png',
    'ios_settings_2x': 'resources/icons/icon-58x58.png',
    'ios_settings_3x': 'resources/icons/android_72x72.png',
    'ios_spotlight': 'resources/icons/icon-40.png',
    'ios_spotlight_2x': 'resources/icons/icon-80.png',

    'android_mdpi': 'resources/icons/android_48x48.png',
    'android_hdpi': 'resources/icons/android_72x72.png',
    'android_xhdpi': 'resources/icons/android_96x96.png',
    'android_xxhdpi': 'resources/icons/icon-144-xxhdpi.png',
    'android_xxxhdpi': 'resources/icons/icon-192-xxxhdpi.png',
});

App.launchScreens({
    'iphone_2x': 'resources/splash/iphone_2x_640x960.png',
    'iphone5': 'resources/splash/iphone5_640x1136.png',
    'iphone6': 'resources/splash/iphone6_750x1334.png',
    'iphone6p_portrait': 'resources/splash/iphone6p_portrait_1242x2208.png',
    'ipad_portrait': 'resources/splash/ipad_portrait_768x1024.png',
    'ipad_portrait_2x': 'resources/splash/ipad_portrait_2x_1536x2048.png',
    'android_mdpi_portrait': 'resources/splash/android_mdpi_portrait_320x480.png',
    'android_hdpi_portrait': 'resources/splash/android_hdpi_portrait_480x800.png',
    'android_xhdpi_portrait': 'resources/splash/android_xhdpi_portrait_720x1280.png'
});

App.accessRule('http://graph.facebook.com/*');
App.accessRule('https://*.googleusercontent.com/*');
App.accessRule('http://*.vk.com/*');
App.accessRule('http://*vk.com/*');
App.accessRule('http://vk.com/*');
App.accessRule('http://*.vk.me/*');
App.accessRule('https://*.vk.me/*');
App.accessRule('https://*.gravatar.com/*');
App.accessRule('http://*.gravatar.com/*');
App.accessRule('https://*.fbcdn.net/*');
//TODO possibly ios build bug
App.accessRule('http://localhost:3000/sockjs/*');
App.accessRule('http://localhost:3000/_timesync/*');
App.accessRule('http://chabtap.ru/*');
App.accessRule('http://game.chabtap.ru/*');

App.setPreference('Orientation', 'portrait');
// в config.xml эти данные попадают, но в AndroidManifest.xml нет, по этому я руками подхачил .meteor/local/cordova-build/platforms/android/AndroidManifest.xml
App.setPreference('android-windowSoftInputMode', 'adjustNothing');

App.configurePlugin('phonegap-plugin-push', {
  SENDER_ID: 12345678
});