relativeTime = (timeAgo) ->
  diff = moment.utc(Date.now() - timeAgo)
  Number(diff.format("s"))

Meteor.methods
  clearAllInvites:  ->
    Invites.update $or: [{to_user: Meteor.userId()},{from_user: Meteor.userId()}],
      $set:
        viewed: true,
      ,
        multi: true

  createGame: (userId_1, userId_2, gameConf) ->
    cells = []
    rows = []
    for i in [1..gameConf.game_rows]
      rowCells = []
      for j in [1..gameConf.game_cols]
        cells.push col:j, row:i
        rowCells.push col:j, row:i
      rows.push rowCells

    delete gameConf._id
    gameConf = _.extend(gameConf, users: [userId_1, userId_2], game_mode: GAME_MODE_STARTING, all_cells: cells, rows: rows, start_time: Date.now())
    gameId = Game.insert gameConf
    gameId

  refreshRemaningTime: (gameId)->
    game = Game.findOne(gameId)
    if game.game_mode == GAME_MODE_PLAYING and not game.seconds_passed
#      console.log relativeTime(game.turn_time)
      if relativeTime(game.turn_time) >= game.seconds_to_turn
        Game.update gameId, $set: seconds_passed: game.seconds_to_turn

  setUserName: (options)->
    Accounts.setUsername(Meteor.userId(), options.username.toLowerCase())

  accountsIsUsernameAvailable: (username)->
    unless username == Meteor.user().username
        Accounts.findUserByUsername(username.toLowerCase()) is undefined
    else
      # это свое же имя
      true

  serverNotification: (text, title) ->
    badge = 1
    Push.send
      from: 'push'
      title: title
      text: text
      badge: badge
      query: {}
    return

  inviteUser: (fromUserId, userId) ->
    username = Meteor.user().username
    if username
      Push.send
        from: 'push'
        title: 'a play invitation'
        text: "#{username} invites you to play chabtap!"
        query:
          userId: userId
      return