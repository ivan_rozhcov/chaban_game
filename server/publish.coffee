Meteor.publish null, ->
  Meteor.users.find {'emails.address': 'robot_1'},
    fields:
      emails: 1
      username: 1
      profile: 1
      status: 1
      isBanned: 1
      number: 1
      won_games: 1
      'services.google.email': 1
      'services.google.picture': 1
      'services.facebook.id': 1
      'services.vk.photo': 1

Meteor.publish 'my_user', ->
  Meteor.users.find @userId,
    fields:
      emails: 1
      username: 1
      profile: 1
      status: 1
      isBanned: 1
      number: 1
      won_games: 1
      'services.google.email': 1
      'services.google.picture': 1
      'services.facebook.id': 1
      'services.vk.photo': 1

Meteor.publish 'lobby', ->
  Lobby.find()

Meteor.publish 'invites', ->
  userIds = _.uniq(_.flatten(Invites.find($or: [
    {
      to_user: @userId
      from_user: $exists: true
    }
    {
      from_user: @userId
      to_user: $exists: true
    }
  ]).map((itm) ->
    [
      itm.to_user
      itm.from_user
    ]
  ), true))
  [
    Invites.find($or: [
      {
        to_user: @userId
        from_user: $exists: true
      }
      {
        from_user: @userId
        to_user: $exists: true
      }
    ])
    Meteor.users.find({ _id: $in: userIds }, fields:
      emails: 1
      username: 1
      profile: 1
      status: 1
      isBanned: 1
      won_games: 1
      'services.google.email': 1
      'services.google.picture': 1
      'services.facebook.id': 1
      'services.vk.photo': 1
      number: 1)
  ]

Meteor.publish 'game', (_id) ->
  game = Game.findOne(_id)
  if game
    userIds = game.users
    [Game.find(_id), Meteor.users.find({ _id: $in: userIds }, fields:
      username: 1
      profile: 1
      status: 1
      isBanned: 1
      won_games: 1
      'services.google.email': 1
      'services.google.picture': 1
      'services.facebook.id': 1
      'services.vk.photo': 1
      number: 1)]


Meteor.publish 'rating', ->
  user = Meteor.users.findOne @userId
  Meteor.users.find $or: [{number: $lt: 11}, {number: $in: [user.number-1,user.number,user.number+1]}],
    fields:
      emails: 1
      username: 1
      profile: 1
      status: 1
      isBanned: 1
      number: 1
      won_games: 1
      'services.google.email': 1
      'services.google.picture': 1
      'services.facebook.id': 1
      'services.vk.photo': 1

Meteor.publish 'invite_users', (query)->
  if query
    Meteor.users.find username: $regex: query.toLowerCase(),
      sort: {number: 1},
      limit: 10
      fields:
        emails: 1
        username: 1
        profile: 1
        status: 1
        isBanned: 1
        number: 1
        won_games: 1
        'services.google.email': 1
        'services.google.picture': 1
        'services.facebook.id': 1
        'services.vk.photo': 1
  else
    Meteor.users.find {_id: $ne: @userId},
      sort: {number: 1},
      limit: 10
      fields:
        emails: 1
        username: 1
        profile: 1
        status: 1
        isBanned: 1
        number: 1
        won_games: 1
        'services.google.email': 1
        'services.google.picture': 1
        'services.facebook.id': 1
        'services.vk.photo': 1