pairArray = (a) ->
  temp = a.slice()
  arr = []
  while temp.length
    arr.push temp.splice(0, 2)
  arr

Lobby.after.update (userId, doc, fieldNames, modifier, options) ->
  for [p1, p2] in pairArray(doc.wait_list)
    if p1 and p2
      console.log 'remove p1 & p2', p1, p2
      Lobby.update doc._id, {$addToSet: {ready: [p1, p2]}, $pull: wait_list: p1}
      Lobby.update doc._id, {$pull: wait_list: p2}
      invite = sendInvite(p1, p2)
      Invites.update invite, $addToSet: invite_accepted: p1

Invites.after.update (userId, doc, fieldNames, modifier, options) ->
  #если оба юзера согласились создаем игру. В нее перекинет шаблон joun game
  if (doc.invite_accepted and doc.to_user in doc.invite_accepted and doc.from_user in doc.invite_accepted) and not doc.game_id
    console.log 'all invites accepted create a game'
    gameConf = {}
    gameConf.game_rows = GAME_ROWS
    gameConf.game_cols = GAME_COLS
    gameConf.tap_times = TAP_TIMES
    gameConf.turn_points_tap = TURN_POINTS_TAP
    gameConf.seconds_to_turn = SECONDS_TO_TURN

    inviteId = doc._id
    #так как вызов асинхронный
    Meteor.call 'createGame', doc.from_user, doc.to_user, gameConf, (err, gameId) ->
      console.log 'game created - set game id to invites'
      #make something to update game
      Game.update(gameId, {$set: test: 1})
      Invites.update inviteId, $set: game_id: gameId

      robot = Meteor.users.findOne('emails.address': 'robot_1', _id: $in: [doc.to_user, doc.from_user])
      if robot
        #в ответ на пришлашение робот должен поставить 3 точки
        randomDelay = GAME_ACTION_DELAY_MSECS + Random.fraction() * GAME_ACTION_ROBOT_DELAY_MSECS
#        console.log randomDelay, doc

        game = Game.findOne(gameId)
        Meteor.setTimeout(->
          randomPoints = []
          while game.tap_times - randomPoints.length > 0
            col = Random.choice([1..game.game_rows])
            row = Random.choice([1..game.game_cols])
            if _.findWhere(randomPoints, {col: col, row: row})
              continue
            else
              randomPoints.push {col: col, row: row}

          query = {}
          query["#{robot._id}_points"] = randomPoints
          Game.update gameId, $set: query

          console.log 'robot set points', query

        , randomDelay)

Invites.after.insert (userId, doc, fieldNames, modifier, options) ->
  robot = Meteor.users.findOne('emails.address': 'robot_1', _id: $in: [doc.to_user, doc.from_user])
  if robot
    console.log 'I\'m a robot!'
    #принимаем приглашение
    Invites.update doc, $addToSet: invite_accepted: robot._id


# вся логика повешена на хуки
# но сейчас добавился таймер который дергается каждый раз, по этому робот делает лишние шаги.
Game.after.update (userId, game, fieldNames, modifier, options) ->
  if game.game_mode == GAME_MODE_STARTING and getUserPoints(game, game.users[0]).length == getUserPoints(game, game.users[1]).length == game.tap_times
    Game.update game._id, $set:
      game_mode: GAME_MODE_PLAYING
      user_turn: Random.choice(game.users)
      turn_time: Date.now()
      seconds_passed: 0
      user_turn_points: 0
      robot_make_turn: false
    return

  else if (game.game_mode == GAME_MODE_PLAYING and (getYouScore(game, game.users[0]) == game.tap_times or getYouScore(game, game.users[1]) == game.tap_times)) or (game.is_force_end and game.game_mode !=  GAME_MODE_ENDED)
    if getYouScore(game, game.users[0]) == game.tap_times
      championId = game.users[0]
      looserId = game.users[1]
    else
      championId = game.users[1]
      looserId = game.users[0]

    delta = getYouScore(game, championId) - getYouScore(game, looserId)

    console.log 'игрок выиграл ', championId

    Meteor.setTimeout(->
      Game.update game._id, $set:
        game_mode: GAME_MODE_ENDED,

      Meteor.users.update championId, $inc: 'won_games': delta
      looser = Meteor.users.findOne(looserId)
      if looser.won_games - delta > 0
        Meteor.users.update looserId, $inc: 'won_games': -delta
      else
        Meteor.users.update looserId, $set: 'won_games': 0

      Meteor.users.update game.users[0], $inc: 'play_games': 1
      Meteor.users.update game.users[1], $inc: 'play_games': 1

    , GAME_ACTION_END_MSECS)
    return

  else if game.game_mode == GAME_MODE_PLAYING and (game.turn_points_tap - game.user_turn_points <= 0 or (game.seconds_to_turn - game.seconds_passed <= 0))
    console.log 'change player', game.robot_make_turn
    newUser = _.find(game.users , (_id) -> _id != game.user_turn)

    Game.direct.update game._id, $set: robot_make_turn: true

    Meteor.setTimeout(->
      Game.update game._id, $set:
        user_turn: newUser
        turn_time: Date.now()
        user_turn_points: 0
        seconds_passed: 0
        robot_make_turn: false
        has_robot_fired: false

    , GAME_ACTION_DELAY_MSECS)
    return

