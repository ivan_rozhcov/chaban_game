Meteor.startup ->
  if not Meteor.users.find('emails.address': 'robot_1').count()
    Accounts.createUser(email: 'robot_1', password: 'robot', profile: name: 'робот', avatar_url: 'https://secure.gravatar.com/avatar/c76c7b9852db835b21e4b844062797d5?s=60&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D60&r=G')
  if not Meteor.users.find('emails.address': 'ivan133@yandex.ru').count()
    Accounts.createUser(email: 'ivan133@yandex.ru', password: 'test', profile: name: 'Иван Тест')
  if not Lobby.find().count()
    Lobby.insert({})
  robot = Meteor.users.findOne('emails.address': 'robot_1')
  unless robot.username
    Meteor.users.update robot._id, $set: username: 'robot'
#  индексы
  Meteor.users._ensureIndex("number": 1)
  Meteor.users._ensureIndex("won_games": 1)
  Meteor.users._ensureIndex("username": 'text')

  Push.Configure({
    apn: {
      certData: Assets.getText('apn-development/PushChatCert.pem'),
      keyData: Assets.getText('apn-development/PushChatKey.pem'),
      passphrase: 'passphrase',
      production: false,
      #gateway: 'gateway.push.apple.com',
    },
    gcm: {
      apiKey: 'apiKey',
      projectNumber: 1234567
    }
    # production: true,
    # 'sound' true,
    # 'badge' true,
    # 'alert' true,
    # 'vibrate' true,
    # 'sendInterval': 15000, Configurable interval between sending
    # 'sendBatchSize': 1, Configurable number of notifications to send per batch
    # 'keepNotifications': false,
  });


  Push.debug = true
  Push.addListener 'error', (err) ->
    console.log arguments
  return