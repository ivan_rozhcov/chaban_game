Accounts.onCreateUser (options, user) ->
  # We still want the default hook's 'profile' behavior.
  if (options.profile)
    user.profile = options.profile;
  user.won_games = 0
  user