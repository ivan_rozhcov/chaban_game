ServiceConfiguration.configurations.remove
  service: "google"
# https://console.developers.google.com/apis/credentials?project=chaban-game
ServiceConfiguration.configurations.insert
  service: "google",
  clientId: "clientId",
  secret: "secret"

# https://vk.com/editapp?id=5365042&section=options
ServiceConfiguration.configurations.remove service: 'vk'
ServiceConfiguration.configurations.insert
  service: 'vk'
  appId: 'appId'
  secret: 'secret'

# https://developers.facebook.com/apps/570960159733557/settings/advanced/
ServiceConfiguration.configurations.remove service: 'facebook'
ServiceConfiguration.configurations.insert
  service: 'facebook'
  appId: 'appId'
  secret: 'secret'