#TODO это временно решение для удаления протухших игр, потом нужно через обычный крон
SyncedCron.add
  name: 'Remove expired games'
  schedule: (parser) ->
# parser is a later.parse object
    parser.text 'every 1 minutes'
  job: ->
    gameIds = Game.find(game_mode: GAME_MODE_PLAYING, start_time: $lt: new Date(new Date().setMinutes(new Date().getMinutes()-3)), {fields: _id: 1}).map((itm) -> itm._id)
    if gameIds
      Invites.update {game_id: {$in: gameIds}},
        $set:
          viewed: true,
        ,
          multi: true

      Game.update({_id: {$in: gameIds}},
        $set:
          game_mode: GAME_MODE_ENDED
        ,
          multi: true
      )

#добавляем роботов в комнату ожидания каждую минуту
SyncedCron.add
  name: 'Add robot to lobby'
  schedule: (parser) ->
    # parser is a later.parse object
    parser.text 'every 1 minutes'
  job: ->
    lobby = Lobby.findOne()
    robotId = Meteor.users.findOne('emails.address': 'robot_1')._id
    if lobby.wait_list and lobby.wait_list.length and robotId not in lobby.wait_list
      Lobby.update lobby._id, $addToSet: wait_list: robotId

#пересчитываем место в рейтинге
SyncedCron.add
  name: ' Recount rating'
  schedule: (parser) ->
    # parser is a later.parse object
    parser.text 'every 1 minutes'
  job: ->
    Meteor.users.find({}, sort: {'won_games':-1}).map (item, index) ->
      number = index + 1
      Meteor.users.update item._id, $set: number: number

SyncedCron.start()
