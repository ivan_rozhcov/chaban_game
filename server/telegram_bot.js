import Telegraf from '/imports/telegraf/lib/index.js'
import { Extra, Markup } from '/imports/telegraf/lib/index.js'

const gameShortName = 'chabtap'
const gameUrl = 'http://chabtap.ru'

const bot = new Telegraf('283266628:AAE964Uykkza9O1YyMFPuH22bKCkRHzKU_g')

bot.command('start', (ctx) => ctx.replyWithGame(gameShortName))

bot.command('chabtap', (ctx) => {
return ctx.replyWithGame(gameShortName, Extra.markup(
  Markup.inlineKeyboard([
    Markup.gameButton('Play now!'),
    Markup.urlButton('Chabtap site', 'http://chabtap.ru/static/')
  ])))
})

bot.gameQuery((ctx) => {
  console.log('Game query:', ctx.callbackQuery.game_short_name)
return ctx.answerCallbackQuery(null, gameUrl)
})

if (!Meteor.isDevelopment){
  bot.startPolling(24)
  console.log('startPolling')
}
