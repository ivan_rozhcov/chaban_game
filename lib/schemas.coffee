SimpleSchema.messages
  alreadyExists: "This username is taken"
  'regEx': [
    { msg: 'Only latin letters and numbers, no special symbols allowed.' }
  ]