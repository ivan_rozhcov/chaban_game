Router.configure
  layoutTemplate: 'Layout'
  notFoundTemplate: 'notFound'
  loadingTemplate: 'loading'
  onBeforeAction: ->
    userId = if Meteor.isClient then Meteor.userId() else @userId
    if userId
      @next()
    else
      if Meteor.isClient then @render('login') else @next()


Router.route '/',
  template: 'menu'
  name: 'game.menu'
  onBeforeAction: ->
    if Meteor.userId()
      user = Meteor.user()
      if user and user.username
        if window.CLIENT_TIMER_ID
          Meteor.clearInterval CLIENT_TIMER_ID
        @next()
      else
        @render 'selectUsername'
    else
      @render 'login'
      @stop()
  waitOn: ->
    [
      Meteor.subscribe('my_user'),
      Meteor.subscribe('invites')
    ]

Router.route '/select-name',
  template: 'selectUsername'
  name: 'game.select_name'
  onBeforeAction: ->
    if this.ready()
      if Meteor.userId()
        @next()
      else
        @render 'login'
        @stop()

@CLIENT_TIMER_ID = null

Router.route '/game/:_id',
  template: 'grid'
  name: 'game.grid'
  data: ->
    Game.findOne(@params._id)
  onBeforeAction: ->
    if Meteor.userId()
      Meteor.call 'clearAllInvites'
      gameId = @params._id

      if window.CLIENT_TIMER_ID
#        console.log 'timer cleared'
        Meteor.clearInterval CLIENT_TIMER_ID

      window.CLIENT_TIMER_ID = Meteor.setInterval((->
        Meteor.call 'refreshRemaningTime', gameId, (error, result) ->
          return
        return
      ), 1000)

      game = Game.findOne(gameId)
      robotId = Meteor.users.findOne('emails.address': 'robot_1')._id
      #если играем с роботом
      if robotId in game.users
        Tracker.autorun ->
          game = Game.findOne(gameId)
          isRobotTurn = game.user_turn == robotId
          if isRobotTurn and not game.has_robot_fired
            #ходит робот должен рендомно поставить точку с рендомной задержкой - и делает пометку, что сходил robot_make_turn
            # тк теперь этот метод вызывается еще и по таймеру
            robot = Meteor.users.findOne(_id: game.user_turn, 'emails.address': 'robot_1')
            turns = game.user_turn_points or 0
            if robot and game.game_mode == GAME_MODE_PLAYING and game.turn_points_tap - turns > 0
#              console.log 'I\'m a robot!'

              randomDelay = Random.fraction() * GAME_ACTION_ROBOT_DELAY_MSECS + 800
              Game.update game._id,  $set: {has_robot_fired: true}
              for turn in [1..game.turn_points_tap]
                Meteor.setTimeout(->
                  firedPoints = game["#{robot._id}_fire_points"] or []
                  randomPoints = []
                  while randomPoints.length < 1
                    col = Random.choice([1..game.game_rows])
                    row = Random.choice([1..game.game_cols])
#                    console.log randomPoints, col, row
                    if _.findWhere(firedPoints, {col: col, row: row})
                      continue
                    else
                      randomPoints.push {col: col, row: row}

#                  console.log randomPoints
                  query = {}
                  query["#{robot._id}_fire_points"] = randomPoints[0]
                  Game.update game._id,
                   $addToSet: query,
                   $set: {has_robot_fired: true}
                   $inc: user_turn_points: 1
                , randomDelay)

                randomDelay += Random.fraction() * GAME_ACTION_ROBOT_DELAY_MSECS


      @next()
    else
      @render 'login'
      @stop()
  waitOn: ->
    [
      Meteor.subscribe('game', @params._id)
      Meteor.subscribe('invites')
    ]

Router.route '/game-configure',
  template: 'configureGame'
  name: 'game.configure'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()

Router.route '/new-game-menu',
  template: 'newGameMenu'
  name: 'game.new_game_menu'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()
  waitOn: ->
    [
      Meteor.subscribe('invites')
      Meteor.subscribe('my_user')
    ]

# это invite
Router.route '/new-game',
  template: 'newGame'
  name: 'game.new_game'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()
  waitOn: ->
    [
      Meteor.subscribe('invites')
      Meteor.subscribe('my_user')
    ]

Router.route '/join-game/:_id',
  template: 'joinGame'
  name: 'game.join_game'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()
  data: ->
    Invites.findOne(@params._id)
  waitOn: ->
    [
      Meteor.subscribe('invites')
    ]

Router.route '/lobby',
  template: 'gameLobby'
  name: 'game.lobby'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()
  data: ->
    Lobby.findOne()
  waitOn: ->
    [
      Meteor.subscribe('invites')
      Meteor.subscribe('lobby')
    ]

Router.route '/score',
  template: 'score'
  name: 'game.score'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()
  waitOn: ->
    [
      Meteor.subscribe('rating')
    ]

Router.route '/tutorial',
  template: 'tutorial'
  name: 'game.tutorial'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
      @stop()
  waitOn: ->
    [
      Meteor.subscribe('my_user')
#      Meteor.subscribe('rating')
    ]
