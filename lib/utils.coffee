@getUserPoints = (game, userId) ->
  objStr = "#{userId}_points"
  game[objStr] or []

@getYouScore = (game, userId) ->
  otherUserId = _.find(game.users , (_id) -> _id != userId)
  selfFiredPoints = "#{userId}_fire_points" or []
  otherSetPoints = "#{otherUserId}_points" or []

  _.filter(game[otherSetPoints], (itm) -> _.findWhere(game[selfFiredPoints], itm)).length

@sendInvite = (userId, fromUserId) ->
  #find Invite
  createdInvites = Invites.find($or: [{to_user: userId, from_user: fromUserId}, {to_user: fromUserId, from_user: userId}], viewed: $ne: true).fetch()
  unless createdInvites.length
    Invites.insert to_user: userId, from_user: fromUserId, invite_accepted: [fromUserId]
  else
    Invites.update createdInvites[0]._id, $addToSet: invite_accepted: fromUserId
    createdInvites[0]._id
    
@getOtherUserId = (game) ->
  _.find(game.users , (_id) -> _id != Meteor.userId())