@usernameSchema = new SimpleSchema
  username:
    type: String
    custom: ->
      if Meteor.isClient and @isSet
        Meteor.call 'accountsIsUsernameAvailable', @value, (error, result) ->
#          console.log 'result', result
          if !result
#            console.log 'notUnique'
            usernameSchema.namedContext('UserNameEdit').addInvalidKeys [ {
              name: 'username'
              type: 'alreadyExists'
            } ]
          return
      return

#    regEx: /^[\p{L}0-9_\-\ ]{3,15}$/
    min: 3
    max: 20
    autoform:
      label: false
      afFieldInput: placeholder: 'Your Nickname using latin and numbers'
      class: 'search_input'
      defaultValue: -> Meteor.user()?.profile?.name
    autoValue: ->
      if @isSet and typeof @value == 'string'
        return @value.toLowerCase()
      return



